﻿#lindrox

一个基于X86的内核半成品。主要实现如下：

1.从实模式跳向保护模式

2.中断管理

3.键盘映射

4.打印字符

5.内存管理（实现虚拟地址映射，最大支持4GB，由此实现了malloc及free函数，可以进行内存申请与回收）

6.一个轻量级文件系统，lxfs，基于B+树，实现了基本的文件目录读写删除等功能，设计中支持显示创建修改时间、权限，一个文件最大允许4TB。设计参考了FAT32、NTFS、EXT等文件系统，以兼容FAT32格式定义了分区表结构，以B+树作为文件管理树，参考EXT4的实现以位图形式对文件数据进行记录。（此文件系统引入lindorx内核后，仅仅实现了文件读写删除，由于时间关系并未进一步实现其他功能）

此程序在windows平台下使用MinGW环境开发，使用bochs作为虚拟机测试。


注：代码久远，现在可能无法正常编译。
